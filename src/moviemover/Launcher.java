/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package moviemover;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Miloune
 */
public class Launcher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length != 0) {
            String racine = args[0];

            if(!racine.substring(racine.length() - 1).matches("/")) {
                racine = racine+"/";
            }

            if(racine.substring(0, 2).matches("\\\\\\\\")) {
                racine = "\\\\" + racine.substring(2, racine.length()).replace("\\", "/");
            } else {
                racine = racine.replace("\\", "/");
            }

            try {
                File racineFolder = new File(racine);
                ArrayList<File> allNodes = new ArrayList<>(Arrays.asList(racineFolder.listFiles()));
                for (File node : allNodes) {
                    if (node.isDirectory()) {
                        File folder = new File(node.getAbsolutePath());
                        ArrayList<File> allNodesFolder = new ArrayList<>(Arrays.asList(folder.listFiles()));

                        if (allNodesFolder.size() == 1) {
                            // Déplace ficher -- Delete folder
                            File fileToMove = allNodesFolder.get(0);
                            if (fileToMove.renameTo(new File(racine + fileToMove.getName()))) {
                                // File moved successful
                                folder.delete();
                                System.out.println("File moved successfully: " + fileToMove.getAbsolutePath());
                            } else {
                                System.err.println("Failed to move: " + fileToMove.getAbsolutePath());
                            }

                        }
                    }
                }
            } catch (NullPointerException npe) {
                System.err.println("Incorrect path");
            }

        } else {
            System.err.println("Please specify the folder path in arg");
        }
    }

}
